@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Application Verification</div>

                <div class="panel-body">
                    <form method="post" action="/app-verification">
                        {{ csrf_field() }}
                        <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                        </div>
                        <div class="form-group">
                        <label for="exampleInputPassword1">Token*</label>
                        <input type="text" name="token" class="form-control" id="exampleInputPassword1" placeholder="Enter your token" required="">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
