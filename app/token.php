<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class token extends Model
{
    protected $fillable =[
        'app_id', 'token', 'ip', 'used_by', 'token_status', 'verified_on'
    ];
    
    
}
